import express from 'express';
import swaggerUI from 'swagger-ui-express';
import yaml from 'yamljs';
import path from 'path';

const swaggerDocuments = yaml.load(
  path.join(__dirname, '../docs/swagger.yaml')
);

export const app = express();

app.use(express.json());

// swagger docs
if (
  process.env.NODE_ENV !== 'production' ||
  process.env.NODE_ENV !== 'staging'
) {
  app.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerDocuments));
}

app.listen(3000, () => console.log('Server is start on port 3000'));
